# Technology Stack
```
2020-0612
feat1

Language
1. TypeScript

Mobile App (iOS and Android)
1. React Native

Server Side
1. AWS Lamdba + NodeJS (serverless function)
2. AWS DynamoDb (NoSQL database)
3. AWS API Gateway
4. Framework for development (serverless)

JavaScript Testing Framework
1. JEST

Source Control
1. GitLab

```

# GitLab-basics Study (1 day)
https://docs.gitlab.com/ee/gitlab-basics/

# Development Machine Setup (1 day)
[Workstation Setup](doc/workstation-setup.md)

# AWS Serverless Tutorial (2-3 days)
1. [AWS Serverless Tutorial](doc/tutorial-sls-aws.md)
2. Create a new project to GitLab
3. Commit and push your project to GitLab


# Test Script -JEST (2 days)
1. please create unit test scirpts by using jest
https://jestjs.io/docs/en/getting-started
2. Commit and push your changes to GitLab

# AWS Serverless Deploy Summary
Steps to checkout tutorial project and deploy to AWS:
1.	gitlab access ready
2.	AWS Account ready and proper access permission granted
3.	Access Key ID and Secret Access Key generated
4.	awscli installed
5.	aws configuration ready
6.	Prepare workspace

# React Native Tutorial (2-3 days)
https://reactnative.dev/docs/getting-started

Please refer to 

"Getting Started" -> "React Native CLI Quickstart" -> "macOS" -> "iOS"

"Getting Started" -> "React Native CLI Quickstart" -> "Windows" -> "Android"


# Mobile Frontend + Serverless Exercise (6-8 days)
1. React Native tutorial is completed
2. Serverless tutorial is completed and server can return list of data
3. [Mobile Frontend Exercise](doc/exercise.md)
4. Please create unit test scirpts by using jest
5. For the mobile project, create a new project to GitLab and push it to GitLab
