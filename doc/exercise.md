Reqired to use AWS Serverless and React Native

The followings are list of required features, UI design or data schema subjects to developer decision.
1. Retrieve records from serverless endpoint and display the list on UI
2. Create client side filter input for filtering records at client side
3. Create client side sorting input for sorting the list by date (accending or decending)
4. Create click actions (click to mark the record as read, click again to mark the record as unread)
5. Submit the read/unread status to serverside and store the status to DynamoDb
6. Serverless function: create API to retreive the count of unread message