---
id: workstation-setup
title: Workstation Setup for Windows
sidebar_label: Workstation Setup
---

## Install and setup

### Install Chocolatey package manager

Open PowerShell (run as Administrator) run the followings

```bat
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

***if http proxy is used:
choco config set proxy http://proxy.ha.org.hk:8080/
choco config set proxyUser xxxxxx
choco config set proxyPassword xxxxxx
```

### Install recommanded packages

Recommanded packages includes :

- Git
- Node.js
- Yarn
- Visual Studio Code
- Gnu On Windows

```bat
choco install git -y
choco install nodejs -y
choco install yarn -y
choco install vscode -y
choco install gow -y
```

### Verify your installation

Open a new command prompt `cmd` and run the followings

```bat
git version
node -v
npm -v
yarn -v
code -v
vim --version
```

## Packages configuration

### Git

```
git config --global user.name "Your Name"
git config --global user.email your.name@ha.org.hk
git config --global http.sslVerify false
```

### Visual Studio Code

1. Recommanded extensions

   - Prettier - Code formatter
   - Bracket Pair Colorizer 2
   - ESLint
   - GitLens
   - REST Client

2. Recommanded settings

   ```json
   {
     "editor.codeActionsOnSave": {
       "source.organizeImports": true
     },
     "editor.formatOnSave": true,
     "editor.renderWhitespace": "all",
     "editor.tabSize": 2,
     "eslint.validate": [
       "javascript",
       "javascriptreact",
       {
         "language": "typescript",
         "autoFix": true
       },
       {
         "language": "typescriptreact",
         "autoFix": true
       }
     ],
     "files.exclude": {
       "**/node_modules": true
     }
   }
   ```
